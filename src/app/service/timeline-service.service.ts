import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { Timeline } from "../components/timeline/timeline";

@Injectable({
  providedIn: 'root'
})
export class TimelineService {
  private apiServerurl = environment.apiBaseUrl;

  constructor(private http: HttpClient) { }

  public getTimeline(): Observable<Timeline[]>{
    return this.http.get<Timeline[]>(`${this.apiServerurl}/timeline/all`);
  }

  public addTimeline(timeline: Timeline): Observable<Timeline>{
    return this.http.post<Timeline>(`${this.apiServerurl}/timeline/add`, timeline);
  }

  public updateTimeline(timeline: Timeline): Observable<Timeline>{
    return this.http.put<Timeline>(`${this.apiServerurl}/timeline/update`, timeline);
  }

  public deleteTimeline(timelineId: number): Observable<void>{
    return this.http.delete<void>(`${this.apiServerurl}/timeline/delete/${timelineId}`);
  }
}
  