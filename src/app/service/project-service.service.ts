import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Project } from '../components/proyect-cards/projects'

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private apiServerurl = environment.apiBaseUrl;

  constructor(private http: HttpClient) { }

  public getProject(): Observable<Project[]>{  
    return this.http.get<Project[]>(`${this.apiServerurl}/project/all`);
  }
  public addProject(project: Project): Observable<Project>{
    return this.http.post<Project>(`${this.apiServerurl}/project/add`, project);
  }

  public updateProject(project: Project): Observable<Project>{
    return this.http.put<Project>(`${this.apiServerurl}/project/update`, project);
  }

  public deleteProject(project: number): Observable<void>{
    return this.http.delete<void>(`${this.apiServerurl}/project/delete/${project}`);
  }
}
