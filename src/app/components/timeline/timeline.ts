export interface Timeline {
    id: number;
    mes?: string;
    anio?: number;
    description?: string;
    puesto?: string;
    timelinecode: string;
}