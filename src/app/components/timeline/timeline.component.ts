import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { TimelineService } from 'src/app/service/timeline-service.service';
import { TokenService } from '../login/security/Service/token.service';
import { Timeline } from './timeline';


@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})

export class TimelineComponent implements OnInit {
  public timelines!: Timeline[];
  derIzq: number =0;
  public editTimeline!: Timeline;
  public deleteTimeline!: Timeline;
  isLogged: boolean = false;
  isAdmin = false;

  constructor(private timelineService: TimelineService, private tokenService: TokenService) { }

  ngOnInit(): void {
    this.getTimelines();
    this.isLogged = this.tokenService.isLogged();
    this.isAdmin = this.tokenService.isAdmin();
  }

  public getTimelines():void {
    this.timelineService.getTimeline().subscribe(
      (response: Timeline[]) =>{
        this.timelines = response;
      },
      (error: HttpErrorResponse) =>{
        alert(error.message);
      }
    )
  }

public isEven(n: number): string {
  if (n % 2 === 0){
    this.derIzq+=1;
    return "Even";
  }else{
    this.derIzq+=1;
    return "Odd";
  }
}
public onOpenModal(timeline : Timeline , mode: string): void{
  const container = document.getElementById('main-cointainer');
  const button = document.createElement('button');
  button.type = 'button';
  button.style.display = 'none';
  button.setAttribute('data-toggle','modal');

  if(mode === 'add'){
    button.setAttribute('data-target','#addTimelineModal');
    
  }
  if(mode === 'edit'){
    this.editTimeline = timeline;
    button.setAttribute('data-target','#editTimelineModal');
    
  }
  if(mode === 'delete'){
    this.deleteTimeline = timeline;
    button.setAttribute('data-target','#deleteTimelineModal');
  }
  container?.appendChild(button);
  button.click();
}

public onAddtimeline(addForm: NgForm):void {
  document.getElementById('add-timeline-modal')?.click();
  this.timelineService.addTimeline(addForm.value).subscribe(
    (response: Timeline) => {
      this.getTimelines();
      addForm.reset();
    },
    (error: HttpErrorResponse) => {
      alert(error.message);
      addForm.reset();
    }
  )
  
}

public onUpdatetimeline(timeline: Timeline):void {
  this.timelineService.updateTimeline(timeline).subscribe(
    (response: Timeline) => {
      // console.log(response);
      this.getTimelines();
    },
    (error: HttpErrorResponse) => {
      alert(error.message);
    }
  )
  
}
public onDeleteTimeline(timelineId: number):void {
  document.getElementById('delete-timeline-modal')?.click();
  this.timelineService.deleteTimeline(timelineId).subscribe(
    (response: void) => {
      console.log(response);
      this.getTimelines();
    },
    (error: HttpErrorResponse) => {
      alert(error.message);
    }
  )
  
}


}
