import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { JwtDto } from '../Entity/jwt-dto';
import { LoginUser } from '../Entity/login-user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authURL = 'https://facufacuportfolio.fly.dev/auth/'

  constructor(private httpClient: HttpClient) { }

  public login(loginUser: LoginUser): Observable<JwtDto> {
    return this.httpClient.post<JwtDto>(this.authURL + 'login', loginUser);
  }
}
