import { Component, OnInit } from '@angular/core';

import { Router, RouterLink } from '@angular/router';
import { LoginUser } from './security/Entity/login-user';
import { AuthService } from './security/Service/auth.service';
import { TokenService } from './security/Service/token.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  isLogged= false;
  isLoginFail= false;
  loginUser!: LoginUser ;
  nombreUser!: string;
  password!: string;
  errMsj!: string;

  
  constructor(
    private tokenService: TokenService, 
    private authService: AuthService, 
    private router: Router){ 
  }

  ngOnInit() {
    this.isLogged= this.tokenService.isLogged();
  }


  onLogin(): void{
    this.loginUser = new LoginUser(this.nombreUser,this.password);
    this.authService.login(this.loginUser).subscribe(data =>{
      
      
      this.tokenService.setToken(data.token);
      window.location.reload();
    },
    err =>{
      this.isLogged = false;
      this.isLoginFail= true;
      this.errMsj = err;
      //console.log(err.Mensaje);
    }
    );
  }
}
