import { Component, OnInit } from '@angular/core';
import { TokenService } from '../login/security/Service/token.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  
})
export class HeaderComponent implements OnInit {
  isLogged!: boolean ;

  constructor(private tokenService: TokenService) { }

  ngOnInit() {
   this.isLogged = this.tokenService.isLogged();
  }

  onLogout(): void {
    this.tokenService.logOut();
    window.location.reload();
  }

}
