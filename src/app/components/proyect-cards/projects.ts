export interface Project {
    id: number; 
    imageReference: string;
    linkRepo: string;
    linkDeploy: string;
    description: string;
    projectcode: string;
}