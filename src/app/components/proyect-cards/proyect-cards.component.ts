import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ProjectService } from 'src/app/service/project-service.service';
import { TokenService } from '../login/security/Service/token.service';
import { Project } from './projects';

@Component({
  selector: 'app-proyect-cards',
  templateUrl: './proyect-cards.component.html',
  styleUrls: ['./proyect-cards.component.css']
})
export class ProyectCardsComponent implements OnInit {
  
  public projects!: Project[];
  public editProjects!: Project;
  public deleteProjects!: Project;
  isLogged: boolean = false;
  isAdmin = false;

  constructor(private projectService: ProjectService, private tokenService: TokenService) { }

  ngOnInit(): void {
    this.getProjects();
    this.isLogged = this.tokenService.isLogged();
    this.isAdmin = this.tokenService.isAdmin();
  }
  public getProjects(): void{
    this.projectService.getProject().subscribe(
      (response: Project[]) =>{
        this.projects = response;
        //console.log(response);
      },
      (error: HttpErrorResponse) =>{
        alert(error.message);
      }
    )
  }
  public onAddProject(addForm: NgForm):void{
    document.getElementById('add-project-modal')?.click();
    this.projectService.addProject(addForm.value).subscribe(
      (res: Project)=>{
        this.getProjects();
        addForm.reset();
      },
      (error: HttpErrorResponse) =>{
        alert(error.message);
        addForm.reset();
      }
    )
  }
  public onUpdateProject(project: Project): void{
    this.projectService.updateProject(project).subscribe(
      (res: Project) =>{
        this.getProjects();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public onDeleteProject(projectId: number): void{
    document.getElementById('delete-project-modal')?.click();
    this.projectService.deleteProject(projectId).subscribe(
      (res: void) =>{
        this.getProjects();
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    )
  }

  public onOpenModal(projects : Project , mode: string): void{
    const container = document.getElementById('main-cointainer');
    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle','modal');
  
    if(mode === 'add'){
      button.setAttribute('data-target','#addProjectModal');
      
    }
    if(mode === 'edit'){
      this.editProjects = projects;
      button.setAttribute('data-target','#editProjectModal');
      
    }
    if(mode === 'delete'){
      this.deleteProjects = projects;
      button.setAttribute('data-target','#deleteProjectModal');
    }
    container?.appendChild(button);
    button.click();
  }
}
