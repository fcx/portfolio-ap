import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { MainSectionComponent } from './components/main-section/main-section.component';
import { ProyectCardsComponent } from './components/proyect-cards/proyect-cards.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { TechnologiesComponent } from './components/technologies/technologies.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { FooterComponent } from './components/footer/footer.component';
import { ContactComponent } from './components/contact/contact.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { TimelineService } from './service/timeline-service.service';
import { ProjectService } from './service/project-service.service';
import { NuevoUserComponent } from './components/login/security/Auth/nuevo-user.component';
import { interceptorProvider } from './components/login/security/Interceptors/portfolio-inteceptors.service';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainSectionComponent,
    ProyectCardsComponent,
    TimelineComponent,
    TechnologiesComponent,
    PageNotFoundComponent,
    FooterComponent,
    ContactComponent,
    LoginComponent,
    HomeComponent,
    NuevoUserComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule
    
  ],
  providers: [TimelineService,ProjectService,interceptorProvider],
  bootstrap: [AppComponent]
})
export class AppModule { }
