import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './components/contact/contact.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { PortfolioGuardsService as guard } from './components/login/security/Guards/portfolio-guards.service';
import { MainSectionComponent } from './components/main-section/main-section.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ProyectCardsComponent } from './components/proyect-cards/proyect-cards.component';
import { TechnologiesComponent } from './components/technologies/technologies.component';
import { TimelineComponent } from './components/timeline/timeline.component';

const routes: Routes = [
  
    {path: '', component : HomeComponent},
    {path: 'about' , component: MainSectionComponent },
    {path: 'work' , component: ProyectCardsComponent,canActivate:[guard] , data:{expectedRole:['admin','user']}},
    {path: 'skills' , component: TechnologiesComponent },
    {path: 'contacto' , component: ContactComponent },
    {path: 'timeline', component: TimelineComponent ,canActivate:[guard] , data:{expectedRole:['admin','user']}},
    {path: 'footer' , component: FooterComponent },
    {path: 'login' , component: LoginComponent },
    {path: '**' , component: PageNotFoundComponent },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes , {enableTracing: false})],
  exports: [RouterModule]
  
})
export class AppRoutingModule { }
